import numpy as np
import time

input_grid = [[5, 0, 0, 0, 8, 0, 0, 4, 9],
              [0, 0, 0, 5, 0, 0, 0, 3, 0],
              [0, 6, 7, 3, 0, 0, 0, 0, 1],
              [1, 5, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 2, 0, 8, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 1, 8],
              [7, 0, 0, 0, 0, 4, 1, 5, 0],
              [0, 3, 0, 0, 0, 2, 0, 0, 0],
              [4, 9, 0, 0, 5, 0, 0, 0, 3]]

global solution  # np.matrix


def is_valid_grid(grid) -> bool:
    for i in range(9):
        row = {}
        column = {}
        block = {}
        row_cube = 3 * (i//3)
        column_cube = 3 * (i % 3)
        for j in range(9):
            if grid[i][j] in set(row)-{0}:
                return False
            row[grid[i][j]] = 1
            if grid[j][i] in set(column)-{0}:
                return False
            column[grid[j][i]] = 1
            rc = row_cube+j//3
            cc = column_cube + j % 3
            if grid[rc][cc] in set(block)-{0}:
                return False
            block[grid[rc][cc]] = 1
    return True


def possible_entry(x, y, n, grid) -> bool:
    for i in range(9):
        if grid[y][i] == n:
            return False
    for i in range(9):
        if grid[i][x] == n:
            return False
    x0 = (x // 3) * 3
    y0 = (y // 3) * 3
    for i in range(3):
        for j in range(3):
            if grid[y0 + i][x0 + j] == n:
                return False
    return True


def solve(grid) -> None:
    for y in range(9):
        for x in range(9):
            if grid[y][x] == 0:
                for n in range(1, 10):
                    if possible_entry(x, y, n, grid):
                        grid[y][x] = n
                        solve(grid)
                        grid[y][x] = 0
                return
    global solution
    solution = np.matrix(grid)


if __name__ == "__main__":
    if is_valid_grid(input_grid):
        start = time.time()
        solve(input_grid)
        end = time.time()
        try:
            print(solution)
            print(f'Puzzle solved in {end-start} seconds')
        except Exception:
            print('Unsolvable puzzle')
    else:
        print("Invalid grid, please verify your input")
